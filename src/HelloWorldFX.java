import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HelloWorldFX extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        //Sets the app name to mine
        primaryStage.setTitle("Kohl Kennedy");        
        StackPane root = new StackPane();
        Text text1 = new Text ("Hello\t\t");
        //Sets the first word to black
        text1.setFill(Color.BLACK);
        Text text2 = new Text ("\tWorld");
        //Sets the second word to red
        text2.setFill(Color.DARKRED);
        //Adds both text to the app
        root.getChildren().addAll(text1,text2);
        primaryStage.setScene(new Scene(root, 200, 200, Color.NAVAJOWHITE));
        //Displays it
        primaryStage.show();
    }
}