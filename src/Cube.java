public class Cube extends Shape {
    //Cube class
    public Cube(int a) {
        super(a);
        int[] vertices = new int[1];
        vertices[0] = a;
    }
    //Returns the area
    public double getArea() {
        return Math.pow(side, 3);
    }
    //Sets the sides
    public void setSides(int n) {
        this.side = vertices[0];
    }
    //Returns the sides
    public double getSide() {
        return side;
    }

}