public class Shape {
    protected double side;
    protected int[] vertices;

    public Shape(int n) {
        vertices = new int[n];
        side = n;
    }
    //Returns the sides
    public double getSide() {
        return side;
    }
    //Sets the sides
    public void setSide(int n) {
        this.side = n;
    }
    //Returns the area
    public double getArea(){
        return side;
    }


}