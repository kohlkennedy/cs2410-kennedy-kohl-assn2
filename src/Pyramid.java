public class Pyramid extends Shape {
    public Pyramid(int a, int b, int c, int d) {
        super(a);
        int[] vertices = new int[4];
        vertices[0] = a;// Base point
        vertices[1] = b;// Base point
        vertices[2] = c;// Base point
        vertices[3] = d;// Top point
    }
    //Sets the sides
    public void setSides(int n) {
        this.side = 4;
    }
    //Returns the sides
    public double getSide() {
        return side;
    }
    //Returns the area
    public double getArea() {
        
        return ((Math.pow(side, 2) / 2) *4);
    }
}