public class Square extends Shape{
    //The vertices to be used
    int[] vertices;
    public Square(int a, int b, int c, int d){
        super(a);
        vertices = new int[4];
        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;
        vertices[3] = d;

    }
    //Returns the side
    public double getSide() {
        return side;
    }
    //Sets the side
    public void setSide(int n) {
        this.side = n;
    }
    public double getArea()
    {
        //Returns the area
        return side * side;
    }
}