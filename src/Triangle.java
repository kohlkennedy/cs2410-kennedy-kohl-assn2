public class Triangle extends Shape
{
    public Triangle(int a, int b, int c)
    {
        super(a);
        int[] vertices = new int[3];
        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;
    }
    //Returns the area
    public double getArea()
    {
    	return (Math.pow(side, 2) / 2);
    }
    //Returns the number of sides
    public double getNumberofSides() {
		return side;
    }
    //Sets the sides
    public void setSides(int n){
        side = n;
    }
}