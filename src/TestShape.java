import java.util.ArrayList;

public class TestShape {

    //The tester class
    public static void main(String args[]) {
        ArrayList<Shape> list = new ArrayList<Shape>(8);
        Pyramid one = new Pyramid(1, 2, 3, 4);
        list.add(one);
        Pyramid two = new Pyramid(2, 3, 4, 5);
        list.add(two);
        Square three = new Square(3, 4, 5, 6);
        list.add(three);
        Square four = new Square(4, 5, 6, 7);
        list.add(four);
        Cube five = new Cube(5);
        list.add(five);
        Cube six = new Cube(6);
        list.add(six);
        Triangle seven = new Triangle(7, 8, 9);
        list.add(seven);
        Triangle eight = new Triangle(8, 9, 10);
        list.add(eight);
        //For loop to show the objects get Area
        for(int i = 0; i< list.size(); i++) 
            System.out.println("Test are of " + list.get(i).toString() + ": " + list.get(i).getArea());
        
    }
}
